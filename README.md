# Test project for a continuous delivery pipeline #

### Summary ###

This is a mutiple module java project using Gradle as a build tool.

Whenever a commit is pushed to this repository, a hook triggers the Jenkins build.

The build is defined with the following:

* Build and Unit tests
* Quality insurance
* Integration tests
* Publish artifact
* Deploy to DEV environment
* Functional tests
* Deploy to UAT environment
* Performance tests
* And a manual step to deploy to PRD environment

[Pipeline view](http://jenkins.cdelattre.com/view/pipeline-view/)

### Build ###

In the root directory run:
```
#!bash

gradlew tasks
```
It will show all defined build steps
test