package library.impl;

import library.bo.Person;

import org.junit.Test;
import static org.junit.Assert.*;

public class LibraryImplTest {

	
	@Test
	public void testGetPersonName() {
		LibraryImpl impl = new LibraryImpl(new Person("Larry"));
		assertEquals("Larry", impl.getPersonName());
	}
}
