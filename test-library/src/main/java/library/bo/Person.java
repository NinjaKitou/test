package library.bo;

/**
 * Business object represents a Person. It defines a name for that person.
 *
 * @author Christophe
 *
 */
public class Person {
    /** Name of the person. */
    private final String name;

    /**
     * Person constructor.
     * return instance of a Person with the specified name.
     * @param iName immutable name configuration.
     */
    public Person(final String iName) {
        this.name = iName;
    }

    /**
     * Getter for the name attribute.
     * @return the Person name.
     */
    public final String getName() {
        return name;
    }
}
