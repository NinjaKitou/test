package library.impl;

import library.bo.Person;

/**
 * Default implementation of the library. Take a Person as an entry.
 *
 * @author Christophe
 *
 */
public class LibraryImpl {

    /** Person instance. */
    private final Person person;

    /**
     * Library constructor.
     * Takes a Person instance (immutable) as argument.
     * @param iPerson the person.
     */
    public LibraryImpl(final Person iPerson) {
        this.person = iPerson;
    }

    /**
     * Get the person name String representation.
     * @return the person name.
     */
    public final String getPersonName() {
        System.out.println(this.person.getName());
        return this.person.getName();
    }

}
