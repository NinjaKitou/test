package ft.resources;

import org.junit.Assert;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Functional test class for Resource web service.
 * @author Christophe
 */
public class PersonResourceFTest {

	@Test
	public void testPerson() {
		Client client = Client.create();

		WebResource webResource = client
				.resource("http://dev.cdelattre.com/test-webapp/rest/test/christophe");

		ClientResponse response = webResource.accept("text/plain").get(
				ClientResponse.class);
		
		String person = response.getEntity(String.class);
		Assert.assertEquals("christophe", person);
	}
}
