package it.resources;

import org.junit.Assert;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Integration test for BuildInfoResource class.
 *
 * @author Christophe
 */
public class BuildInfoResourceITest {

    /**
     * Tests the Resource web service.
     */
    @Test
    public void testBuildInfo() {
        Client client = Client.create();

        WebResource webResource = client
                .resource("http://localhost:8080/test-webapp/rest/buildinfo");

        ClientResponse response = webResource.accept("text/plain").get(
                ClientResponse.class);

        String buildinfo = response.getEntity(String.class);
        Assert.assertNotNull(buildinfo);
    }
}
