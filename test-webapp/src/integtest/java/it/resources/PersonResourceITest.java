package it.resources;

import org.junit.Assert;
import org.junit.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

/**
 * Integration test for PersonResource class.
 *
 * @author Christophe
 */
public class PersonResourceITest {

    /**
     * Tests the Resource web service.
     */
    @Test
    public void testPerson() {
        Client client = Client.create();

        WebResource webResource = client
                .resource("http://localhost:8080/test-webapp/rest/test/chris");

        ClientResponse response = webResource.accept("text/plain").get(
                ClientResponse.class);

        String person = response.getEntity(String.class);
        Assert.assertEquals("chris", person);
    }
}
