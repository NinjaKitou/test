package webapp;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.servlet.ServletContext;

import junit.framework.Assert;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;

import org.junit.Test;
import org.junit.runner.RunWith;

import utils.PropertyUtils;

@RunWith(JMockit.class)
public class BuildInfoResourceTest {

    /**
     * Test the getBuildInfo method from BuildInfoResource.
     * By default the build info file does not exist.
     * It should thow on exception.
     */
    @Test
    public void testGetBuildInfo() {
        BuildInfoResource resource = new BuildInfoResource();
        try {
            String buildInfo = resource.getBuildInfo();
            Assert.fail();
        } catch (Exception ex) {
        }
    }

    /**
     * Mock the build info file.
     * In this case, the test should not
     * throw an exception.
     * @throws IOException in case the file is not found
     */
    @Test
    public void testGetBuildInfoWithFileMocked(
            @Mocked PropertyUtils propUtils) throws IOException {

        new Expectations() {
            String test = "test";
            byte[] bytes = test.getBytes();
            InputStream istream = new ByteArrayInputStream(bytes);
            {
                PropertyUtils.getInputStreamFromContextFile(
                        (ServletContext) any, (String) any); result = istream;
            }
        };

        BuildInfoResource resource = new BuildInfoResource();
        String buildInfo = resource.getBuildInfo();
        Assert.assertNotNull(buildInfo);
        Assert.assertEquals("test", buildInfo);
    }

}
