package webapp;

import junit.framework.Assert;

import org.junit.Test;

import webapp.PersonResource;

/**
 * Unit test for PersonResource class.
 * @author Christophe
 *
 */
public class PersonResourceTest {

    /**
     * Test the getName method from PersonResource.
     */
    @Test
    public void testGetName() {
        PersonResource resource = new PersonResource();
        String resourceName = resource.getName("unit test");
        Assert.assertEquals("unit test", resourceName);
    }

}
