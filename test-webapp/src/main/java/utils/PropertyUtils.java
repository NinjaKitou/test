package utils;

import java.io.InputStream;

import javax.servlet.ServletContext;

/**
 * Define utility functions to manage properties.
 *
 * @author Christophe
 */
public final class PropertyUtils {

    /**
     * Private constructor as it is a Utility class.
     */
    private PropertyUtils() {
    }

    /**
     * Get the inputstream of the file found in
     * the ServletContext.
     * @param context the context
     * @param filename the file to read
     * @return the file InputStream.
     */
    public static InputStream getInputStreamFromContextFile(
            final ServletContext context, final String filename) {
        return context.getClassLoader().getResourceAsStream(filename);
    }
}
