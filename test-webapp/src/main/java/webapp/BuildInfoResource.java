package webapp;

import java.io.InputStream;
import java.util.Scanner;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;

import utils.PropertyUtils;

/**
 * Class providing build info web services.
 *
 * @author Christophe
 *
 */
@Path("/buildinfo")
public class BuildInfoResource {

    /** The execution context. */
    @Context
    private ServletContext context;

    /**
     * Web service returning the build info file as a String.
     *
     * @return build info
     */
    @GET
    @Produces("text/plain")
    public final String getBuildInfo() {
        InputStream inputStream = PropertyUtils.getInputStreamFromContextFile(
                context, "build-info.properties");

        Scanner scanner = new Scanner(inputStream, "UTF-8");
        scanner.useDelimiter("\\A");

        String buildInfo = scanner.next();
        scanner.close();
        return buildInfo;
    }
}
