package webapp;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import library.bo.Person;
import library.impl.LibraryImpl;

/**
 * Class providing Person web services.
 *
 * @author Christophe
 *
 */
@Path("/test")
public class PersonResource {

    /**
     * Web service returning the provided name as a String.
     *
     * @param name
     *            input name
     * @return the provided input name
     */
    @GET
    @Produces("text/plain")
    @Path("{name}")
    public final String getName(@PathParam("name") final String name) {
        LibraryImpl impl = new LibraryImpl(new Person(name));
        return impl.getPersonName();
    }
}
